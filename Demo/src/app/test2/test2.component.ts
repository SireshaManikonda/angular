import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrl: './test2.component.css',
})
export class Test2Component implements OnInit {
  person: any;
  constructor() {
    // alert('Constructor');
    this.person = {
      id: 101,
      name: 'Siresha',
      avg: 50.55,
      address: { flatNo: 803, city: 'Hyderabad', state: 'Telangana' },
      hobbies: ['Movies', 'Music', 'Swimming'],
    };
  }
  buttonSubmit() {
    alert('Button Clicked');
    console.log(this.person);
  }
  ngOnInit() {}
}
