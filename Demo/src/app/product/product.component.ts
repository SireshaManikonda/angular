import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
})
export class ProductComponent implements OnInit {
  products = [
    {
      name: '',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      price: 29.99,
      image: 'https://via.placeholder.com/150',
    },
    {
      name: 'Product 2',
      description:
        'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      price: 39.99,
      image: 'https://via.placeholder.com/150',
    },
    {
      name: 'Product 3',
      description:
        'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      price: 39.99,
      image: 'https://via.placeholder.com/150',
    },
    {
      name: 'Product 4',
      description:
        'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      price: 39.99,
      image: 'https://via.placeholder.com/150',
    },
    {
      name: 'Product 5',
      description:
        'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      price: 39.99,
      image: 'https://via.placeholder.com/150',
    },
    {
      name: 'Product 6',
      description:
        'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      price: 39.99,
      image: 'https://via.placeholder.com/150',
    },
    // Add more products as needed
  ];

  constructor() {}

  ngOnInit(): void {}
}
