import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { EmpComponentComponent } from './emp-component/emp-component.component';
import { GetEmpByIdComponent } from './get-emp-by-id/get-emp-by-id.component';
import { ProductComponent } from './product/product.component';
import { authGuard } from './auth.guard';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  {
    path: 'showemps',
    canActivate: [authGuard],
    component: EmpComponentComponent,
  },
  {
    path: 'showempbyid',
    canActivate: [authGuard],
    component: GetEmpByIdComponent,
  },
  { path: 'products', canActivate: [authGuard], component: ProductComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
