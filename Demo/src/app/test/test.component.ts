import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css',
})
export class TestComponent implements OnInit {
  id: number;
  name: string;
  avg: number;
  address: any;
  hobbies: any;
  constructor() {
    this.id = 101;
    this.name = 'Siresha';
    this.avg = 50.52;
    this.address = { flatNo: 803, city: 'Hyderabad', state: 'Telanagana' };
    this.hobbies = ['Reading Novels', 'Listening Music', 'Eating', 'Sleeping'];
  }
  ngOnInit() {}
}
